-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2019 at 04:24 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_detail`
--

CREATE TABLE `table_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_invent` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `jumlah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_detail`
--

INSERT INTO `table_detail` (`id_detail_pinjam`, `id_invent`, `id_peminjaman`, `jumlah`) VALUES
(69, 61, 85, '25'),
(70, 62, 86, '5'),
(71, 63, 87, '5'),
(72, 64, 88, '5'),
(73, 65, 89, '25'),
(74, 61, 90, '25'),
(75, 61, 91, '1'),
(76, 61, 92, '1'),
(77, 61, 93, '1'),
(78, 61, 94, '9'),
(79, 61, 95, '10'),
(80, 61, 96, '1'),
(81, 61, 97, '1');

-- --------------------------------------------------------

--
-- Table structure for table `table_invent`
--

CREATE TABLE `table_invent` (
  `id_invent` int(11) NOT NULL,
  `kode_barang` varchar(200) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `kondisi_barang` varchar(50) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `tgl_register` date NOT NULL,
  `keterangan` text NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_invent`
--

INSERT INTO `table_invent` (`id_invent`, `kode_barang`, `nama_barang`, `id_jenis`, `kondisi_barang`, `id_ruang`, `jumlah`, `tgl_register`, `keterangan`, `id_petugas`) VALUES
(61, 'BRG082019001', 'Laptop', 4, 'Baik', 9, 198, '2019-04-08', 'Tersedia', 1),
(62, 'BRG082019002', 'Bola Futsal', 5, 'Baik', 16, 25, '2019-04-08', 'Tersedia', 1),
(63, 'BRG082019003', 'Bola Volly', 5, 'Baik', 16, 25, '2019-04-08', 'Tersedia', 1),
(64, 'BRG082019004', 'Bola Basket', 5, 'Baik', 16, 25, '2019-04-08', 'Tersedia', 1),
(65, 'BRG082019005', 'Headset', 4, 'Baik', 9, 50, '2019-04-08', 'Tersedia', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_jenis`
--

CREATE TABLE `table_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(25) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_jenis`
--

INSERT INTO `table_jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(4, 'Alat Elektronik', 'AL', 'Digunakan untuk keperluan Elektronik'),
(5, 'Alat Olahraga', 'AO', 'Digunakan untuk keperluan Olahraga.'),
(6, 'Alat Tetap', 'AT', 'Tidak dapat digunakan oleh orang lain.');

-- --------------------------------------------------------

--
-- Table structure for table `table_pegawai`
--

CREATE TABLE `table_pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_pegawai`
--

INSERT INTO `table_pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(5, 'Diana Nurhasanah', '123456789000000000', 'Jln.HM Syarifuddin No.23 Pillar II RT 01/01 Gg.Damai II'),
(6, 'Indriani', '123456789011111111', 'Jln.HM Syarifuddin No.23 Pillar II RT 01/01 Gg.Damai II'),
(11, 'Hasan Basri', '123456789022222222', 'Jauh Pake HELM Level 3'),
(12, 'Indra Maulana', '089854470900000000', 'Jln.HM Syarifuddin No.23'),
(13, 'Rinaldi Ardiansyah', '123456789033333333', 'Cibinong Gardu'),
(14, 'Roro Siti Fatimah', '123456789044444444', 'Jln.HM Syarifuddin No.20 Gg.Damai 1 RT 01/01 '),
(15, 'Rifky Afrizal', '123456789066666666', 'Jl.Villa Ciomas');

-- --------------------------------------------------------

--
-- Table structure for table `table_peminjaman`
--

CREATE TABLE `table_peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tgl_pinjam` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_kembali` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_peminjaman` varchar(50) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_peminjaman`
--

INSERT INTO `table_peminjaman` (`id_peminjaman`, `tgl_pinjam`, `tgl_kembali`, `status_peminjaman`, `id_petugas`, `id_pegawai`) VALUES
(85, '2019-04-08 09:52:33', '2019-04-08 09:52:33', 'Telah Dikembalikan', 1, 0),
(86, '2019-04-08 09:52:36', '2019-04-08 09:52:36', 'Telah Dikembalikan', 1, 0),
(87, '2019-04-08 09:52:38', '2019-04-08 09:52:38', 'Telah Dikembalikan', 1, 0),
(88, '2019-04-08 09:52:40', '2019-04-08 09:52:40', 'Telah Dikembalikan', 1, 0),
(89, '2019-04-08 09:52:43', '2019-04-08 09:52:43', 'Telah Dikembalikan', 1, 0),
(90, '2019-04-08 12:28:21', '2019-04-08 12:28:21', 'Telah Dikembalikan', 1, 0),
(91, '2019-04-08 12:45:12', '2019-04-08 12:45:12', 'Telah Dikembalikan', 1, 0),
(92, '2019-04-08 13:01:55', '2019-04-08 13:01:55', 'Telah Dikembalikan', 1, 0),
(93, '2019-04-09 04:27:37', '2019-04-09 04:27:37', 'Telah Dikembalikan', 1, 0),
(94, '2019-04-10 02:15:16', '2019-04-10 02:15:16', 'Telah Dikembalikan', 1, 0),
(95, '2019-04-10 02:18:39', '2019-04-10 02:18:39', 'Telah Dikembalikan', 1, 0),
(96, '2019-04-10 02:20:41', '0000-00-00 00:00:00', 'Sedang Dipinjam', 0, 15),
(97, '2019-04-10 02:21:05', '0000-00-00 00:00:00', 'Sedang Dipinjam', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `table_petugas`
--

CREATE TABLE `table_petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `user_key` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `level` enum('Admin','Operator') NOT NULL DEFAULT 'Operator'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_petugas`
--

INSERT INTO `table_petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `email`, `user_key`, `status`, `level`) VALUES
(1, 'Bang Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'programerdesain@gmail.com', '', 1, 'Admin'),
(5, 'Bang Operator', 'operator', '4b583376b2767b923c3e1da60d10de59', 'operator@gmail.com', '', 1, 'Operator'),
(6, 'Rizqi Fadhillah', 'Rizqi', '2d817ef0ef6206da3d06c752e71728a4', 'rizqifadhillah@gmail.com', '', 1, 'Admin'),
(7, 'Dudi Iskandar', 'Dudi', '64963c7e25f4a1e8ed74c8871be96b5d', 'dudiiskandar@gmail.com', '', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `table_ruang`
--

CREATE TABLE `table_ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(25) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_ruang`
--

INSERT INTO `table_ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(9, 'Laboratorium RPL', 'LRPL', 'LAB RPL ini dibagi menjadi dua yaitu LAB RPL 1 dan LAB  RPL 2, yang digunakan untuk kegiatan RPL'),
(10, 'Studio Animasi', 'SANM', 'Studio Animasi ini dibagi menjadi dua kelas yaitu Studio Animasi 1 dan Studio Animasi 2, yang digunakan untuk kegiatan Animasi'),
(11, 'Studio Brodcasting', 'SBC', 'Studio ini ditempatkan dilantai 2, yang digunakan untuk kegiatan Brodcasting'),
(12, 'Bengkel Pengelasan', 'BPL', 'Bengkel ini digunakan untuk kegiatan pengelasan.'),
(13, 'Bengkel Teknik Kendaraan ', 'BTKR', 'Bengkel ini digunakan untuk kegiatan Otomotif.'),
(14, 'Ruang Osis', 'RO', 'Ruang Osis ini berada dilantai 2 didekat tangga. dan digunakan untuk kegiatan Osis'),
(15, 'Ruang TU', 'RTU', 'Berada didekat Lapangan, yang digunakan untuk Administrasi Sekolah'),
(16, 'Ruang Guru', 'RGU', 'Ruang Guru ini ditempatkan disamping Masjid SMKN 1 Ciomas, yang digunakan untuk para Guru.'),
(17, 'Masjid Darul Muttaqin', 'TMA', 'Masjid ini digunakan untuk beribadah Siswa,Guru Beserta Staff lainnya.'),
(18, 'Ruang BK', 'RBK', 'Ruang ini ditempatkan dilantai 2, yang digunakan untuk Guru-guru BK'),
(19, 'Ruang UKS', 'UKS', 'Ruangan ini digunakan untuk perlengkapan medis, seperti obat-obatan, perban, betadine dan lain-lainnya.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_detail`
--
ALTER TABLE `table_detail`
  ADD PRIMARY KEY (`id_detail_pinjam`);

--
-- Indexes for table `table_invent`
--
ALTER TABLE `table_invent`
  ADD PRIMARY KEY (`id_invent`);

--
-- Indexes for table `table_jenis`
--
ALTER TABLE `table_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `table_pegawai`
--
ALTER TABLE `table_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `table_peminjaman`
--
ALTER TABLE `table_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `table_petugas`
--
ALTER TABLE `table_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `table_ruang`
--
ALTER TABLE `table_ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_detail`
--
ALTER TABLE `table_detail`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `table_invent`
--
ALTER TABLE `table_invent`
  MODIFY `id_invent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `table_jenis`
--
ALTER TABLE `table_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `table_pegawai`
--
ALTER TABLE `table_pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `table_peminjaman`
--
ALTER TABLE `table_peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `table_petugas`
--
ALTER TABLE `table_petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `table_ruang`
--
ALTER TABLE `table_ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
