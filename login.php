<?php
	session_start();
	include 'config/config.php';
	if(isset($_SESSION['login_admin'])){
		header("location:pages/dashboard.php");
	}
	else if(isset($_SESSION['login_operator'])){
		header("location:pages/dashboard.php");
	}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>IMS | Login Page</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- <base href="http://localhost/ujikom/"> -->
	<base href="https://appims.000webhostapp.com/">
	<link rel="icon" href="dist/img/icon.png" type="image/gif">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/util.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/main.css">
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-50 p-b-30">
				<form action="" method="POST" class="login100-form validate-form">
					<span class="login100-form-title p-b-30">
						<img src="dist/img/IMS-LOGO.png" width="70%"> 
					</span>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Field is required">
						<select class="input100 select2" name="level">
							<option hidden selected>Pilih Hak Akses</option>
							<option value="Admin">Admin</option>
							<option value="Operator">Operator</option>
						</select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-users"></span>
						</span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
						<input class="input100" type="text" name="username" placeholder="Username" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-user"></span>
						</span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>
					<div class="w-full m-b-16">
						<a class="txt1 hov1" href="forgot-pass.php">
							Lupa Password?							
						</a>
					</div>
					<div class="container-login100-form-btn">
						<button type="submit" name="login" class="login100-form-btn">
							Masuk
						</button>
					</div>
					<div class="text-center w-full p-t-50">
						<span class="txt1">
							Masuk sebagai Pegawai?
						</span>
						<a class="txt1 bo1 hov1" href="login2.php">
							Klik disini							
						</a>
					</div>
				</form>
				<?php
					if (isset($_POST['login'])) {
						$username = $_POST['username'];
						$password = md5($_POST['password']);
						$level = $_POST['level'];
						if($level == "Admin"){
							$select = mysqli_query($config,"SELECT * FROM table_petugas WHERE username='$username' && password='$password' && level='Admin'");
							$cek = mysqli_num_rows($select);
							if($cek == 1){
								$bagi = mysqli_fetch_array($select);
								$_SESSION['login_admin']=$bagi['id_petugas'];
								$_SESSION['nama_petugas']=$bagi['nama_petugas'];
								$_SESSION['username']=$bagi['username'];
								$_SESSION['level']=$bagi['level'];
								echo"<script>alert('Yee, Login Berhasil!');window.location.assign('pages/dashboard.php');</script>";
							}else{
								echo"<script>alert('Username or Password Tidak Ditemukan.');</script>";
							}
						}else if($level=="Operator"){
							$select = mysqli_query($config,"SELECT * FROM table_petugas WHERE username='$username' && password='$password' && level='Operator'");
							$cek = mysqli_num_rows($select);
							if($cek == 1){
								$bagi = mysqli_fetch_array($select);
								$_SESSION['login_operator']=$bagi['id_petugas'];
								$_SESSION['nama_petugas']=$bagi['nama_petugas'];
								$_SESSION['username']=$bagi['username'];
								$_SESSION['level']=$bagi['level'];
								echo"<script>alert('Yee, Login Berhasil!');window.location.assign('pages/dashboard.php');</script>";
							}else{
								echo"<script>alert('Username or Password Tidak Ditemukan.');</script>";
							}
						}else{
							echo"<script>alert('Anda Belum Memilih Hak Akses.');</script>";
						}
					}
				?>
			</div>
		</div>
	</div>
	<script src="bower_components/beautiful-login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="bower_components/beautiful-login/vendor/bootstrap/js/popper.js"></script>
	<script src="bower_components/beautiful-login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="bower_components/beautiful-login/vendor/select2/select2.min.js"></script>
	<script src="bower_components/beautiful-login/js/main.js"></script>
</body>
</html>
<?php
}
?>