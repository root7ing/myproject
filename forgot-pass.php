<?php  
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include 'config/config.php';
include 'vendor/function.php';
include 'vendor/autoload.php';
$userKey = isset($_GET['user_key']) ? $_GET['user_key'] : null;
$sql = mysqli_query($config,"SELECT * FROM table_petugas WHERE user_key='$userKey'");
$row = mysqli_fetch_assoc($sql);
if ($userKey) {
	if ($row) {
		$status = $row['status'];
		$changePassword = true;
	}else{
		$message = [
	        'title' => 'Gagal!',
	        'txt' => 'Gagal, Kode verifikasi salah!',
	        'type' => 'error',
	        'href' => '#'
	    ];	
	}
}
if (isset($_POST['ForgotPass'])) {
	$username = $_POST["username"];
	$user_key = md5(rndStr());
	$sqlCheck = mysqli_query($config,"SELECT * FROM table_petugas WHERE username = '$username' OR email = '$username'");
	$row = mysqli_fetch_assoc($sqlCheck);
	$user_key = uniqid(md5(rndStr()));
	if ($row) {
		$user_keyNow = $row['user_key'];
		$emailTo = $row['email'];
		if ($row['status'] == 0) {
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Gagal, Akun anda belum diaktivasi!',
		        'type' => 'error',
		        'href' => '#'
		    ];
		}
		elseif (empty($message)) {
				$id_petugas = $row['id_petugas'];
				$query = mysqli_query($config,"UPDATE table_petugas SET user_key = '$user_key' WHERE id_petugas = '$id_petugas'");
				// $query = mysqli_query($sql);
				if ($query) {
				    $subject = 'Token untuk mengganti Password : '.$username;
				    $deskripsi = "Link Forgot Password: http://localhost/ujikom/forgot-pass.php?user_key=".$user_key;
					// SERVER SETTINGS
					$mail = new PHPMailer(); // Passing `true` enables exceptions
					$mail->isSMTP();   // Set mailer to use SMTP
				    $mail->isHTML(true);
				    $mail->SMTPDebug = 0; // Enable verbose debug output
				    $mail->SMTPAuth = true; // Enable SMTP authentication
				    $mail->SMTPSecure = 'tls';  // Enable TLS encryption, `ssl` also accepted
				    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
				    $mail->Username = 'im1808000@gmail.com';  // SMTP username
				    $mail->Password = 'v455w0rd'; // SMTP password
				    $mail->Port = 587; // Ubah ke 587, bila anda menggunakan gmail
				    $mail->setFrom('im1808000@gmail.com');
					$mail->addAddress($emailTo); // Add a recipient
				    $mail->Subject = $subject;
				    $mail->Body    = $deskripsi;
				    if($mail->send()){
						echo '<script>alert("Pesan Terkirim, Silahkan Cek Email");</script>';
						// echo 'Pesan Terkirim!';
					} else {
						echo '<script>alert("Pesan Gagal Terkirim!");</script>';
						// echo 'Email Error: ' . $mail->ErrorInfo;
				    }
				}		
				
		}
	}else{
	    $message = [
	        'title' => 'Gagal!',
	        'txt' => 'Gagal, Username / Email tidak ditemukan!',
	        'type' => 'error',
	        'href' => 'forgot-pass.php'
	    ];			
	}
}
if (isset($_POST['changepassword'])) {
	$password = $_POST["password"];
	$re_password = $_POST['re-password'];	
	if ($password != $re_password) {
		   echo "<script>alert('Password Dan Re-type Password Harus Sama!');</script>";
		   $message="gagal";
	}
	$User_key = $row['user_key'];
	if (empty($User_key)) {
	    echo "<script>alert('Kode Verifikasi Salah!');</script>";	
		   $message="gagal";
	}

	if ($message!="gagal") {
		$id_petugas = $row['id_petugas'];
		$password = md5($password); // Verifikasi Password
		$query = mysqli_query($config,"UPDATE table_petugas SET password = '$password', user_key = '' WHERE id_petugas = '$id_petugas'");
		if ($query) {
		    echo "<script>alert('Berhasil Ganti Password');window.location.assign('login.php');</script>"; 
		}else{
		    echo "<script>alert('Gagal ganti Password!');</script>";
		}		
	}
}
?>
<?php
	include "config/config.php";
	session_start();
	if(isset($_SESSION['login_admin'])){
		echo"<script>window.location.assign('pages/dashboard');</script>";
	}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>IMS | Forgot Password Page</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="http://localhost/ujikom/">
	<link rel="icon" href="dist/img/icon.png" type="image/gif">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/util.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/main.css">
</head>
<body>
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-50 p-b-30">
				<form action="" method="POST" class="login100-form validate-form">
					<?php if (isset($changePassword)): ?>
					<span class="login100-form-title p-b-30">
						<img src="dist/img/IMS-LOGO.png" width="70%"> 
					</span>
					<p>Ganti Password untuk Akun : <b><?=$row['username'];?></b></p>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Field is required">
						<input class="input100" type="password" name="password" placeholder="Masukan Password" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Field is required">
						<input class="input100" type="password" name="re-password" placeholder="Masukan re-Password" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>
					<div class="container-login100-form-btn p-b-50">
						<button type="submit" name="changepassword" class="login100-form-btn">
							Ganti Password
						</button>
					</div>
					<?php else: ?>
					<span class="login100-form-title p-b-30">
						<img src="dist/img/IMS-LOGO.png" width="70%"> 
					</span>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Field is required">
						<input class="input100" type="text" name="username" placeholder="Masukan Email anda" autocomplete="off" value="<?php isset($username) ? $username : '' ?>">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span>
					</div>
					<div class="container-login100-form-btn p-b-50">
						<button type="submit" name="ForgotPass" class="login100-form-btn">
							Lupa Password
						</button>
					</div>
					<?php endif ?>
				</form>
			</div>
		</div>
	</div>
	<script src="bower_components/beautiful-login/vendor/jquery/jquery-3.2.1.min.js"></script>
 	<script src="bower_components/beautiful-login/vendor/bootstrap/js/popper.js"></script>
	<script src="bower_components/beautiful-login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="bower_components/beautiful-login/vendor/select2/select2.min.js"></script>
	<script src="bower_components/beautiful-login/js/main.js"></script>
</body>
</html>	
<?php
}
?>