  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>I</b>MS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="dist/img/IMS.png"></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="dist/img/user8-128x128.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image">
                <p> 
                  <?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?>
                  <small><?php echo isset($_SESSION['login_admin'])?"Status : ".$_SESSION['level']:(isset($_SESSION['login_operator'])?"Status : ".$_SESSION['level']:" NIP : ".$_SESSION['nip']);?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-primary btn-flat"><i class="fa fa-sign-out"></i> Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>
            <?php echo isset($_SESSION['login_admin'])?$_SESSION['nama_petugas']:(isset($_SESSION['login_operator'])?$_SESSION['nama_petugas']:$_SESSION['nama_pegawai']);?>
          </p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-primary"></i> <?php echo isset($_SESSION['login_admin'])?"Status : ".$_SESSION['level']:(isset($_SESSION['login_operator'])?"Status : ".$_SESSION['level']:" NIP : ".$_SESSION['nip']);?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header text-center">MAIN NAVIGATION</li>
        <!-- Optionally, you can add icons to the links -->
        <li <?php if($pages == "dashboard") echo "class='active'";?>><a href="pages/dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <?php
          if(isset($_SESSION['login_admin'])){
        ?>
        <li <?php if($pages == "inventarisir") echo "class='active'";?>><a href="pages/data_barang.php"><i class="fa fa-file-text"></i> <span>Inventarisir</span></a></li>
        <li <?php if($pages == "peminjaman") echo "class='active'";?>><a href="pages/data_peminjaman.php"><i class="fa fa-file-text"></i> <span>Peminjaman</span></a></li>
        <li <?php if($pages == "pengembalian") echo "class='active'";?>><a href="pages/data_pengembalian.php"><i class="fa fa-file-text"></i> <span>Pengembalian</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th-large"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($pages == "petugas") echo "class='active'";?>><a href="pages/data_petugas.php"><i class="fa fa-circle-o text-aqua"></i> Data Petugas</a></li>
            <li <?php if($pages == "pegawai") echo "class='active'";?>><a href="pages/data_pegawai.php"><i class="fa fa-circle-o text-aqua"></i> Data Pegawai</a></li>
            <li <?php if($pages == "jenis") echo "class='active'";?>><a href="pages/data_jenis.php"><i class="fa fa-circle-o text-aqua"></i> Data Jenis</a></li>
            <li <?php if($pages == "ruang") echo "class='active'";?>><a href="pages/data_ruang.php"><i class="fa fa-circle-o text-aqua"></i> Data Ruang</a></li>
          </ul>
        </li>
        <?php
          }else if(isset($_SESSION['login_operator'])){
        ?>
        <li <?php if($pages == "peminjaman") echo "class='active'";?>><a href="pages/data_peminjaman.php"><i class="fa fa-file-text"></i> <span>Peminjaman</span></a></li>
        <li <?php if($pages == "pengembalian") echo "class='active'";?>><a href="pages/data_pengembalian.php"><i class="fa fa-file-text"></i> <span>Pengembalian</span></a></li>
        <?php
          }else if(isset($_SESSION['login_peminjam'])){
        ?>
          <li <?php if($pages == "peminjaman") echo "class='active'";?>><a href="pages/data_peminjaman.php"><i class="fa fa-file-text"></i> <span>Peminjaman</span></a></li>
          <li <?php if($pages == "pengembalian") echo "class='active'";?>><a href="pages/data_pengembalian.php"><i class="fa fa-file-text"></i> <span>Pengembalian</span></a></li>
        <?php
          }
        ?>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>