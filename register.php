<?php
	session_start();
    include 'config/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>IMS | Register Page</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- <base href="http://localhost/ujikom/"> -->
	<base href="https://appims.000webhostapp.com/">
	<link rel="icon" href="dist/img/icon.png" type="image/gif">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/util.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/main.css">
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-50 p-b-30">
				<form action="" method="POST" class="login100-form validate-form">
					<span class="login100-form-title p-b-30">
						<img src="dist/img/IMS-LOGO.png" width="70%"> 
					</span>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Input is required">
						<input class="input100" type="text" name="nip" placeholder="Masukan NIP" autocomplete="off" maxLength="18">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-pencil"></span>
						</span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Input is required">
						<input class="input100" type="text" name="nama_pegawai" placeholder="Masukan Nama Lengkap" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-user"></span>
						</span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Input is required">
						<input class="input100" type="text" name="alamat" placeholder="Masukan Alamat" autocomplete="off"></textarea>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-map"></span>
						</span>
					</div>
					<div class="container-login100-form-btn">
						<button type="submit" name="register" class="login100-form-btn">
							Daftar
						</button>
                    </div>
                    <div class="text-center w-full p-t-25">
						<span class="txt1">
							<!-- Belum terdaftar? -->
						</span>
					</div>
				</form>
				<?php
					if (isset($_POST['register'])) {
						$nip = $_POST['nip'];
						$nama_pegawai = $_POST['nama_pegawai'];
						$alamat = $_POST['alamat'];
                        $sql = "SELECT * FROM table_pegawai WHERE nip='$nip'";
                        $query = $config->query($sql);
                        if($query->num_rows != 0){
                            echo "<script>alert('NIP sudah terdaftar!');window.location.assign('register.php');</script>";
                        } else {
                            if(!$nip || !$nama_pegawai || !$alamat){
                                echo "<script>alert('Masih ada data yang kosong!');window.location.assign('register.php');</script>";
                            } else{
                                $data = "INSERT into table_pegawai VALUES (NULL,'$nama_pegawai','$nip','$alamat')";
                                $simpan = $config->query($data);
                                if($simpan){
                                    echo "<script>alert('Pendaftaran Sukses, Silahkan Login');window.location.assign('login2.php');</script>";
                                } else{
                                    echo "<script>alert('Pendaftaran Gagal!');window.location.assign('register.php');</script>";
                                }
                            }
                        }
					}
				?>
			</div>
		</div>
	</div>
  <script src="bower_components/beautiful-login/vendor/jquery/jquery-3.2.1.min.js"></script>
  <script src="bower_components/beautiful-login/vendor/bootstrap/js/popper.js"></script>
  <script src="bower_components/beautiful-login/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="bower_components/beautiful-login/vendor/select2/select2.min.js"></script>
  <script src="bower_components/beautiful-login/js/main.js"></script>
</body>
</html>