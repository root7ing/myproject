<?php
  session_start();
  $pages = "dashboard";
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam']) ){
?>
<!DOCTYPE html>
<html>
<head>
  <?php include "../layouts/links.php";?>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
  <div class="wrapper">
    <!-- Main Header -->
    <?php include "../layouts/header.php";?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
          <small>Inventory Management Software</small>
        </h1>
      </section>
      <!-- Main content -->
      <!-- <section class="content container-fluid">        
        <div class="row">
        </div>
      </section> -->
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include "../layouts/footer.php";?>
  </div>
  <!-- ./wrapper -->
  <?php include "../layouts/scripts.php";?>
</body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('../login2.php');</script>";
  }else{
    echo"<script>window.location.assign('../login.php');</script>";
  }
}
?>