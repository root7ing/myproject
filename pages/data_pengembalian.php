<?php
  session_start();
  include "../config/config.php";
  $pages = "pengembalian";
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam'])){
?>
<!DOCTYPE html>
<html>
<head>
  <?php include("../layouts/links.php") ?>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
  <div class="wrapper">
    <!-- Main Header -->
    <?php include("../layouts/header.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Data Pengembalian Barang
          <small>Inventory Management Software</small>
        </h1>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">
        <div class="box">
          <div class="box-header">
            <a href="pages/export/file_pengembalian.php" target="_blank" class="btn btn-default btn-flat"><i class="fa fa-print"></i> Print</a>
            <div class="btn-group">
              <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
              Export <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="pages/export/pengembalian-to-excel.php"><i class="fa fa-file-excel-o text-green"></i> to EXCEL</a></li>
              </ul>
            </div>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                      <th class="text-center tableNumber">No.</th>
                      <th class="text-center">Kode Barang</th>
                      <th class="text-center">Nama Barang</th>
                      <th class="text-center">Nama Peminjam</th>
                      <th class="text-center">Tanggal Kembali</th>
                      <th class="text-center">Jumlah Barang</th>
                      <th class="text-center">Status Peminjaman</th>
                      <th class="text-center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  include("../config/config.php");
                  $no=0;
                  if(isset($_SESSION['login_admin'])){
                    $where="p.id_petugas=".$_SESSION['login_admin'];
                    $nama_petugas=$_SESSION['nama_petugas'];
                  }else if(isset($_SESSION['login_operator'])){
                    $where="p.id_petugas=".$_SESSION['login_operator'];
                    $nama_petugas=$_SESSION['nama_petugas'];
                  }else if(isset($_SESSION['login_peminjam'])){
                    $where="p.id_pegawai=".$_SESSION['login_peminjam'];
                    $nama_petugas=$_SESSION['nama_pegawai'];
                  }
                  $query = mysqli_query($config,"SELECT p.*,pt.nama_petugas,i.nama_barang,i.kode_barang,d.jumlah,i.id_invent FROM table_peminjaman p LEFT JOIN table_detail d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN table_petugas pt ON p.id_petugas=pt.id_petugas LEFT JOIN table_invent i ON d.id_invent=i.id_invent WHERE $where ORDER BY p.id_peminjaman ASC") or die (mysqli_error($config));
                  if (mysqli_num_rows($query) == 0) {
                      echo '<tr><td class="text-center" colspan="8">Tidak ada Data!</td></tr>';
                  }else{
                      while ($data = mysqli_fetch_array($query)) {
                      $no++;
                ?>
                <tr>
                    <td class="text-center"><?php echo $no; ?></td>          
                    <td><?php echo $data['kode_barang']; ?></td>
                    <td><?php echo $data['nama_barang']; ?></td>
                    <td><?php echo $nama_petugas ?></td>          
                    <td><?php echo $data['tgl_kembali']; ?></td>
                    <td><?php echo $data['jumlah']; ?></td>
                    <td>
                      <?php
                        if($data['status_peminjaman']=='Sedang Dipinjam'){
                          echo $data['status_peminjaman'];
                        }else if($data['status_peminjaman']=='Telah Dikembalikan'){
                          echo $data['status_peminjaman'];
                        }
                      ?>
                    </td>
                    <?php
                      if($data['status_peminjaman']=='Sedang Dipinjam'){
                    ?>
                    <td class="text-center">
                      <a href="pages/crud/proses_pengembalian_barang.php?id_petugas<?=$data['id_petugas'];?>&id_peminjaman=<?=$data['id_peminjaman'];?>&id_invent=<?=$data['id_invent'];?>&jumlah=<?=$data['jumlah'];?>" class="label label-danger"><i class="fa fa-arrow-left"></i> Kembalikan</a>
                    </td>
                    <?php 
                    }else{
                      echo "<td></td>";
                    ?>
                    <?php
                    }
                    ?>
                </tr>
                <?php
                }
                }
                ?>
                </tbody>  
              </table>
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include("../layouts/footer.php");?>
  </div>
  <!-- ./wrapper -->
  <?php include("../layouts/scripts.php");?>
</body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('login2.php');</script>";
  }else{
    echo"<script>window.location.assign('login.php');</script>";
  }
}
?>