<html>
    <style>
        *{
            font-size: 12px;
            font-family : 'Arial',sans-serif;
        }
        table{
            width:100%;
            max-width: 100%;
            border:1px solid #eee;
            border-collapse: collapse;
        }
        thead>tr>th, tbody>tr>td {
            border:1px solid #eee;
            padding: 5px;
            /* text-align: center; */
        }
        .text-center{
            text-align: center !important;
        }
        h1{
            font-size : 16px;
            margin-bottom : 25px;
            text-transform : uppercase;
        }
        button{
            /* color: #fff; */
            background-color : #fff;
            border: 1px solid #eee;
            padding: 10px;
            margin-bottom: 10px;
            /* text-align: center; */
            width: 100px;
            cursor: pointer;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        }
        @media print{
            @page {size: landscape}
            *{
                font-size: 12px;
                font-family : 'Arial',sans-serif;
            }
            table{
                width:100%;
                max-width: 100%;
                /* border:1px solid #eee; */
                border-collapse: collapse;
            }
            thead>tr>th, tbody>tr>td {
                border:1px solid #eee;
                padding: 5px;
                /* text-align: center; */
            }
            .text-center{
                text-align: center !important;
            }
            h1{
                font-size : 16px;
                margin-bottom : 25px;
                text-transform : uppercase;
            }
            .no-print{
                display: none;
            }
        }
    </style>
    <body>
        <button type="button" onclick="window.print()" class="no-print">Print</button>
        <h1 class="text-center">Data Barang</h1>
        <table border="1">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Jenis Barang</th>
                    <th>Kondisi Barang</th>
                    <th>Ruang</th>
                    <th>Tanggal Register</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include("../../config/config.php");
                $no=0;
                $query = mysqli_query($config,"SELECT table_invent.*,table_jenis.nama_jenis,table_ruang.nama_ruang,table_petugas.nama_petugas FROM table_invent LEFT JOIN table_jenis ON table_invent.id_jenis=table_jenis.id_jenis LEFT JOIN table_ruang ON table_invent.id_ruang=table_ruang.id_ruang LEFT JOIN table_petugas ON table_invent.id_petugas=table_petugas.id_petugas ORDER BY kode_barang") or die (mysqli_error());
                if (mysqli_num_rows($query) == 0) {
                    echo '<tr><td colspan="11">Tidak ada Data!</td></tr>';
                }else{
                    while ($data = mysqli_fetch_array($query)) {
                    $no++;
                ?>
                <tr>
                    <td class="text-center"><?php echo $no; ?></td>          
                    <td><?php echo $data['kode_barang']; ?></td>          
                    <td><?php echo $data['nama_barang']; ?></td>
                    <td><?php echo $data['jumlah']; ?></td>
                    <td><?php echo $data['nama_jenis']; ?></td>
                    <td><?php echo $data['kondisi_barang']; ?></td>
                    <td><?php echo $data['nama_ruang']; ?></td>
                    <td><?php echo $data['tgl_register']; ?></td>
                    <td><?php echo $data['keterangan']; ?></td>
                </tr>
                <?php
                }
                }
                ?>
            </tbody>  
        </table>
    </body>
</html>