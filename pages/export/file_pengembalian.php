<html>
    <style>
        *{
            font-size: 12px;
            font-family : 'Arial',sans-serif;
        }
        table{
            width:100%;
            max-width: 100%;
            border:1px solid #eee;
            border-collapse: collapse;
        }
        thead>tr>th, tbody>tr>td {
            border:1px solid #eee;
            padding: 5px;
            /* text-align: center; */
        }
        .text-center{
            text-align: center !important;
        }
        h1{
            font-size : 16px;
            margin-bottom : 25px;
            text-transform : uppercase;
        }
        button{
            /* color: #fff; */
            background-color : #fff;
            border: 1px solid #eee;
            padding: 10px;
            margin-bottom: 10px;
            /* text-align: center; */
            width: 100px;
            cursor: pointer;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        }
        @media print{
            @page {size: landscape}
            *{
                font-size: 12px;
                font-family : 'Arial',sans-serif;
            }
            table{
                width:100%;
                max-width: 100%;
                /* border:1px solid #eee; */
                border-collapse: collapse;
            }
            thead>tr>th, tbody>tr>td {
                /* border:1px solid #eee; */
                padding: 5px;
                /* text-align: center; */
            }
            .text-center{
                text-align: center !important;
            }
            h1{
                font-size : 16px;
                margin-bottom : 25px;
                text-transform : uppercase;
            }
            .no-print{
                display: none;
            }
        }
    </style>
    <body>
        <button type="button" onclick="window.print()" class="no-print">Print</button>
        <h1 class="text-center">Data Pengembalian Barang</h1>
        <table border="1">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Nama Peminjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Jumlah Barang</th>
                    <th>Status Peminjaman</th>
                </tr>
            </thead>
            <tbody>
            <?php
                session_start();
                include("../../config/config.php");
                $no=0;
                if(isset($_SESSION['login_admin'])){
                    $where="p.id_petugas=".$_SESSION['login_admin'];
                }else if(isset($_SESSION['login_operator'])){
                    $where="p.id_petugas=".$_SESSION['login_operator'];
                }else if(isset($_SESSION['login_peminjam'])){
                    $where="p.id_pegawai=".$_SESSION['login_peminjam'];
                }
                $query = mysqli_query($config,"SELECT p.*,pt.nama_petugas,i.nama_barang,i.kode_barang,d.jumlah,i.id_invent FROM table_peminjaman p LEFT JOIN table_detail d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN table_petugas pt ON p.id_petugas=pt.id_petugas LEFT JOIN table_invent i ON d.id_invent=i.id_invent WHERE $where ORDER BY p.id_peminjaman DESC") or die (mysqli_error());
                if (mysqli_num_rows($query) == 0) {
                    echo '<tr><td class="text-center" colspan="8">Tidak ada Data!</td></tr>';
                }else{
                    while ($data = mysqli_fetch_array($query)) {
                    $no++;
            ?>
            <tr>
                <td class="text-center"><?php echo $no; ?></td>          
                <td><?php echo $data['kode_barang']; ?></td>
                <td><?php echo $data['nama_barang']; ?></td>
                <td><?php echo $data['nama_petugas']; ?></td>          
                <td><?php echo $data['tgl_kembali']; ?></td>
                <td><?php echo $data['jumlah']; ?></td>
                <?php
                    if($data['status_peminjaman']=='Sedang Dipinjam'){
                ?>
                <td class="text-center">
                    <a href="pages/crud/proses_pengembalian_barang.php?id_petugas<?=$data['id_petugas'];?>&id_peminjaman=<?=$data['id_peminjaman'];?>&id_invent=<?=$data['id_invent'];?>&jumlah=<?=$data['jumlah'];?>" class="label label-danger"><i class="fa fa-arrow-left"></i> Kembalikan</a>
                </td>
                <?php 
                }else{
                    echo "<td>Telah Dikembalikan</td>";
                ?>
                <?php
                }
                ?>
            </tr>
            <?php
            }
            }
            ?>
            </tbody>  
        </table>
    </body>
</html>