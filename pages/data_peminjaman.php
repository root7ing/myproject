<?php
  session_start();
  include "../config/config.php";
  $pages = "peminjaman";
  if(isset($_SESSION['login_admin']) || isset($_SESSION['login_operator']) || isset($_SESSION['login_peminjam'])){
?>
<!DOCTYPE html>
<html>
<head>
  <?php include("../layouts/links.php") ?>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
  <div class="wrapper">
    <!-- Main Header -->
    <?php include("../layouts/header.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Peminjaman Barang
          <small>Inventory Management Software</small>
        </h1>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">
        <div class="row">
          <?php
            $query = mysqli_query($config, "SELECT * FROM table_invent");
            while($show=mysqli_fetch_array($query)){
          ?>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="ion ion-android-archive"></i></span>
              <div class="info-box-content">
                <span class="info-box-text"><?php echo $show['nama_barang'];?></span>
                <span class="info-box-number">Stok : <?php echo $show['jumlah'];?></span>
                <span href="#" data-target="#ModalAdd<?=$show['id_invent'];?>" data-toggle="modal" class="info-box-footer">Pinjam <?php echo $show['nama_barang'];?> <i class="ion ion-android-open"></i></span>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php
      $query = mysqli_query($config, "SELECT * FROM table_invent");
      while($show=mysqli_fetch_array($query)){
    ?>
    <div id="ModalAdd<?=$show['id_invent'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Form Pinjam</h4>
          </div>
          <div class="modal-body">
            <form action="pages/crud/proses_pinjam_barang.php" name="modal_popup" enctype="multipart/form-data" method="POST">
              <input type="hidden" name="id_invent" value="<?php echo $show['id_invent'];?>">
              <div class="form-group">
                <?php if(isset($_SESSION['login_admin'])){
                  $id_petugas=$_SESSION['login_admin'];
                  $nama_petugas=$_SESSION['nama_petugas'];
                }else if(isset($_SESSION['login_operator'])){
                  $id_petugas=$_SESSION['login_operator'];
                  $nama_petugas=$_SESSION['nama_petugas'];
                }else if(isset($_SESSION['login_peminjam'])){
                  $id_petugas=$_SESSION['login_peminjam'];
                  $nama_petugas=$_SESSION['nama_pegawai'];
                }
                ?>
                <label for="nama_petugas">Nama Peminjam</label>
                <input type="text" name="nama_petugas" class="form-control" autocomplete="off" disabled value="<?php echo $nama_petugas?>">
                <input type="hidden" name="id_petugas" value="<?php echo $id_petugas?>">
              </div>
              <div class="form-group">
                <label>Nama Barang</label>
                <input type="text" name="nama_barang" class="form-control" autocomplete="off" disabled value="<?php echo $show['nama_barang'];?>">
              </div>
              <div class="form-group">
                <label for="jumlah">Jumlah Barang</label>
                <input type="number" name="jumlah" class="form-control" autocomplete="off" max="<?php echo $show['jumlah'];?>" value="1" min="1"/>
              </div>
              <div class="modal-footer">
                <button type="reset" class="btn btn-default btn-flat" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Batal</button>
                <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </form>
          </div>   
        </div>
      </div>
    </div>
    <?php 
    }
    ?>
    <!-- Main Footer -->
    <?php include("../layouts/footer.php");?>
  </div>
  <!-- ./wrapper -->
  <?php include("../layouts/scripts.php");?>
</body>
</html>
<?php
}else{
  if(isset($_SESSION['login_peminjam'])){
    echo"<script>window.location.assign('login2.php');</script>";
  }else{
    echo"<script>window.location.assign('login.php');</script>";
  }
}
?>