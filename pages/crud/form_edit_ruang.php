<?php
    include "../../config/config.php";
	$id_ruang=$_GET['id_ruang'];
	$modal=mysqli_query($config,"SELECT * FROM table_ruang WHERE id_ruang='$id_ruang'");
	while($r=mysqli_fetch_array($modal)){
?>
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Edit Ruang</h4>
        </div>
        <div class="modal-body">
        	<form action="pages/crud/proses_edit_ruang.php" name="modal_popup" enctype="multipart/form-data" method="POST">        		
                <div class="form-group">
                	<label for="kode_ruang">Kode Ruang</label>
                    <input type="hidden" name="id_ruang" class="form-control" value="<?php echo $r['id_ruang']; ?>" />
     				<input type="text" name="kode_ruang" class="form-control" value="<?php echo $r['kode_ruang']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="nama_ruang">Nama Ruang</label>
     				<input type="text" name="nama_ruang" class="form-control" value="<?php echo $r['nama_ruang']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="keterangan">Keterangan</label>       
     				<textarea name="keterangan" class="form-control"><?php echo $r['keterangan']; ?></textarea>
                </div>
	            <div class="modal-footer">
	                <button type="reset" class="btn btn-default btn-flat" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Batal</button>
	                <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
	            </div>
            </form>
            <?php } ?>
            </div>
        </div>
    </div>
</div>