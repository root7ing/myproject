<?php
	session_start();
	include 'config/config.php';
	if(isset($_SESSION['login_peminjam'])){
		header("location:pages/dashboard.php");
	}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>IMS | Login Page</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="http://localhost/ujikom/">
	<!-- <base href="http://appims.000webhostapp.com/"> -->
	<link rel="icon" href="dist/img/icon.png" type="image/gif">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/util.css">
	<link rel="stylesheet" type="text/css" href="bower_components/beautiful-login/css/main.css">
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-50 p-b-30">
				<form action="" method="POST" class="login100-form validate-form">
					<span class="login100-form-title p-b-30">
						<img src="dist/img/IMS-LOGO.png" width="70%"> 
					</span>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "NIP is required">
						<input class="input100" type="text" name="nip" placeholder="Masukan NIP anda" autocomplete="off" maxLength="18">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-pencil"></span>
						</span>
					</div>
					<div class="container-login100-form-btn">
						<button type="submit" name="login" class="login100-form-btn">
							Masuk
						</button>
					</div>
					<div class="text-center w-full p-t-50">
						<span class="txt1">
							Belum terdaftar?
						</span>
						<a class="txt1 bo1 hov1" href="register.php">
							Daftar sekarang							
						</a>
					</div>
				</form>
				<?php
					if (isset($_POST['login'])) {
						$nip = $_POST['nip'];
                        $select = mysqli_query($config,"SELECT * FROM table_pegawai WHERE nip='$nip'");
                        $cek = mysqli_num_rows($select);
                        if($cek == 1){
                            $bagi = mysqli_fetch_array($select);
                            $_SESSION['login_peminjam']=$bagi['id_pegawai'];
                            $_SESSION['nip']=$bagi['nip'];
                            $_SESSION['nama_pegawai']=$bagi['nama_pegawai'];
                            echo"<script>alert('Yee, Login Berhasil!');window.location.assign('pages/dashboard.php');</script>";
                        }else{
                            echo"<script>alert('NIP Tidak Ditemukan.');</script>";
                        }
					}
				?>
			</div>
		</div>
	</div>
  <script src="bower_components/beautiful-login/vendor/jquery/jquery-3.2.1.min.js"></script>
  <script src="bower_components/beautiful-login/vendor/bootstrap/js/popper.js"></script>
  <script src="bower_components/beautiful-login/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="bower_components/beautiful-login/vendor/select2/select2.min.js"></script>
  <script src="bower_components/beautiful-login/js/main.js"></script>
</body>
</html>
<?php
}
?>